<?php
  header('Content-type: application/json');
  $debug = 'no';
  error_reporting(E_ALL);
  ini_set('display_errors', 1);

if (isset($_GET['user-session-id'])) {
  $userSessionID = $_GET['user-session-id'];
} else {
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }
  $userSessionID = session_id();
}

$data = json_decode(file_get_contents("php://input"), true);
$receivedData = $data;
// echo json_encode($receivedData);



// Initialize the database variables
$dsn = 'mysql:host=dalog.cvxwjgsz8jk3.us-east-1.rds.amazonaws.com;dbname=da_forms';
$dbUsername = 'awsadmin';
$dbPassword = 'yL6H7LL1';


if( isset($data['userId']) ) {
  $userId = $data['userId'];
} else {
  $userId = "";
}

if( isset($data['userName']) ) {
  $userName = $data['userName'];
} else {
  $userName = "";
}

// First Name
if( isset($data['userFirstName']) ) {
  $userFirstName= $data['userFirstName'];
} else {
  $userFirstName = "";
}

// Last Name
if( isset($data['userLastName']) ) {
  $userLastName = $data['userLastName'];
} else {
  $userLastName = "";
}

if( isset($data['userEmail']) ) {
  $userEmail = $data['userEmail'];
} else {
  $userEmail = "";
}

if( isset($data['salesContact']) ) {
  $salesContact = $data['salesContact'];
} else {
  $salesContact = "";
}

if( isset($data['type']) ) {
  $type = $data['type'];
} else {
  $type = "";
}

if( isset($data['course']) ) {
  $course = $data['course'];
} else {
  $course = "";
}



if ($debug == 'yes') {
  echo "<br>We will attempt to connect to the database<br>";
}

// Connect to the database
try {
  $db = new PDO($dsn, $dbUsername, $dbPassword);
} catch (PDOException $e) {
  if($debug == 'yes') {
    echo "<br>Unable to connect<br>";
    print $e->getMessage();
  } else {
    error_log($e->getMessage(), 0);
  }
  exit;
}

// Insert the data
try {
    $sqlInsert = "INSERT INTO sales_logging"
      . " (userId,
          fist_name,
          last_name,
          userName,
          userEmail,
          salesContact,
          type,
          course)"
      . " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    $insertResults = $db->prepare($sqlInsert);
    $insertResults->bindParam(1, $userId, PDO::PARAM_STR);
    $insertResults->bindParam(2, $userFirstName, PDO::PARAM_STR);
    $insertResults->bindParam(3, $userLastName, PDO::PARAM_STR);
    $insertResults->bindParam(4, $userName, PDO::PARAM_INT);
    $insertResults->bindParam(5, $userEmail, PDO::PARAM_STR);
    $insertResults->bindParam(6, $salesContact, PDO::PARAM_STR);
    $insertResults->bindParam(7, $type, PDO::PARAM_STR);
    $insertResults->bindParam(8, $course, PDO::PARAM_STR);
    if($insertResults->execute())
    {
      $res="Data Inserted Successfully:";
      echo json_encode($res);
    }
    else {
      $error="Not Inserted,Some Problem occurred.";
      echo json_encode($error);
    }

  }
  catch (PDOException $e) {
    if($debug == 'yes') {
      echo "<br>Insert didn't work<br>";
      print $e->getMessage();
      echo json_encode($e);
    } else {
      error_log($e->getMessage(), 0);
    }
    exit;
  }

// Close the database
$insertResults = null;
$db = null;

// send success message back to the page that requested this one


?>




<?php

// FIELDS

// Name

if ($userFirstName === '' || $userLastName === '') {
  $nameToMail = $userEmail;
} else {
  $nameToMail = "$userFirstName $userLastName";
}

  $firstName = strip_tags(trim($nameToMail));
  $saleRepEmail = strip_tags(trim("$salesContact"));






   date_default_timezone_set('America/New_York');
   // echo date_default_timezone_get();
   $currenttime = date('h:i:s:u');
   list($hrs,$mins,$secs,$msecs) = split(':',$currenttime);
   $nowTime = "$hrs:$mins:$secs";




if ($type === 'enter') {
  $email_subject = "Sales prospect $firstName logged into Preview Site";

  $email_body = "$firstName ($userEmail) logged into the LMS Courses preview site at $nowTime EST";

} elseif ($type === 'view') {
  $email_subject = "Sales prospect $firstName ($userEmail) previewed $course";

  $email_body = " $firstName previewed $course on the LMS Courses Preview Site at $nowTime EST";
}





// Email The Form

require 'PHPMailerAutoload.php';

$mail = new PHPMailer;


// $htmlEmail = <<<EOD
// <div>SOME HTML</div>
// EOD;

// $htmlEmail = file_get_contents('responder.html');

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

// TODO Need smtp info for sending from IT to send email
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'email-smtp.us-east-1.amazonaws.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'AKIAJOUPKCQO2CHJQIYA';                 // SMTP username
$mail->Password = 'AmlbL4BP5L/jjwG2aO4iYLmOK7FSyYCVdU4oqJLF61O1';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->setFrom('DAelearning@driversalert.com', 'LMS Courses Prospect Activity');
//$mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
// $mail->addAddress('incoming@driversalert.com');               // Name is optional
$mail->addAddress("$saleRepEmail");               // Name is optional
$mail->addReplyTo('DAelearning@driversalert.com');
// $mail->addCC('jstevens@driversalert.com');
$mail->addCC('MKroll@driversalert.com');
$mail->addBCC('jjoseph@driversalert.com');

// $mail->addBCC('jake@driversalert.com');
// $mail->addBCC('jstevens@driversalert.com');


//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(false);                                  // Set email format to HTML

$mail->Subject = "$email_subject";
$mail->Body    = "$email_body";
//$mail->AltBody = 'Sorry, No code...';
// $mail->SMTPDebug = 2;
if(!$mail->send()) {
    echo 'Message could not be sent. ';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}
